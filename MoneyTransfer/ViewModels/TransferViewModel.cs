﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class TransferViewModel
    {
        [Required]
        [Display(Name = "CashNumber")]
        public string CashNumber { get; set; }

        [Required]
        [Display(Name = "Balance")]
        [Range(1, 10000000, ErrorMessage = "Incorrect Summ")]
        public int Balance { get; set; }
    }
}
