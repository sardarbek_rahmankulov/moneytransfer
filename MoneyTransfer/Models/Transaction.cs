﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }
        public DateTime ActionDate { get; set; }
        public int Summ { get; set; }

        
        public string FromUser { get; set; }
        public string ToUser { get; set; }

        public User User { get; set; }
    }
}
