﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class CashIdGenerator
    {
        public string GenerateRandomCashId()
        {
            Random rnd = new Random();
            string r = rnd.Next(100000, 1000000).ToString("D6");
            if (r.Distinct().Count() == 1)
            {
                r = GenerateRandomCashId();
            }
            return r;
        }
    }
}
