﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;

namespace MoneyTransfer.Controllers
{
    public class HomeController : Controller
    {
        private readonly IViewLocalizer _localizer;

        private readonly ApplicationContext context;

        public HomeController(ApplicationContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpGet]
        public IActionResult AddBalance()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddBalance(TransferViewModel model)
        {
            User user = context.Users.FirstOrDefault(u => u.UserName == model.CashNumber);
            if (user != null && model.Balance > 0)
            {
                user.Balance += model.Balance;
                Transaction transaction = new Transaction()
                {
                    ActionDate = DateTime.Now,
                    Summ = model.Balance,
                    FromUser = "Anonymous",
                    ToUser = user.UserName
                };
                context.Transactions.Add(transaction);
                context.SaveChanges();
                return View("Index");
            }
            return View();
        }
    }
}
