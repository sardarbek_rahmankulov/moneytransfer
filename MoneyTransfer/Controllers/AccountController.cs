﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationContext context;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this.context = context;
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                CashIdGenerator cashIdGenerator = new CashIdGenerator();
                string UserId = cashIdGenerator.GenerateRandomCashId();
                User user = new User { Email = model.Email, UserName = UserId, CashNumber = UserId, Balance = 1000 };
                // добавляем пользователя
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // установка куки
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result =
                    await _signInManager.PasswordSignInAsync(model.CashNumber, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    // проверяем, принадлежит ли URL приложению
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            // удаляем аутентификационные куки
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Transfer()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Transfer(TransferViewModel model)
        {
            User user = context.Users.FirstOrDefault(u => u.UserName == model.CashNumber);
            User currentUser = await _userManager.FindByNameAsync(User.Identity.Name);
            Transaction transaction = new Transaction()
            {
                ActionDate = DateTime.Now,
                Summ = model.Balance,
                FromUser = currentUser.UserName,
                ToUser = user.UserName
            };

            if (currentUser.Balance >= model.Balance && user !=null)
            {
                user.Balance += model.Balance;
                currentUser.Balance -= model.Balance;
                context.Transactions.Add(transaction);
                context.SaveChanges();
                return RedirectToAction("Transaction");
            }
            return View("ErrorTransfer");
        }

        public async Task<IActionResult> Transaction(List<Transaction> transactions)
        {
            User currentUser = await _userManager.FindByNameAsync(User.Identity.Name);
            transactions = context.Transactions
                .Where(t => t.FromUser == currentUser.CashNumber ||
                            t.ToUser == currentUser.CashNumber).
                OrderByDescending(t => t.ActionDate).ToList();
            return View(transactions);
        }
    }
}
